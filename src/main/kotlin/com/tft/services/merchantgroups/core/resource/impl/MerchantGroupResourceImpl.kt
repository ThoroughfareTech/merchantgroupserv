package com.tft.services.merchantgroups.core.resource.impl
import com.tft.services.merchantgroups.api.model.Merchant
import com.tft.services.merchantgroups.api.model.MerchantGroup
import com.tft.services.merchantgroups.api.model.MerchantGroupPage
import com.tft.services.merchantgroups.api.resources.MerchantGroupResource
import com.tft.services.merchantgroups.core.data.model.Consumer
import com.tft.services.merchantgroups.core.data.repository.ConsumerRepository
import com.tft.services.merchantgroups.core.data.repository.MerchantGroupMemberRepository
import com.tft.services.merchantgroups.core.data.repository.MerchantGroupRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.helpers.NOPLogger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import java.math.BigInteger
import javax.inject.Inject
import javax.inject.Named
import javax.ws.rs.Path
import javax.ws.rs.core.Response

@Named
@Path("{user-id}/groups")
class MerchantGroupResourceImpl (@Inject val consumerRepo: ConsumerRepository, @Inject val merchantGroupRepo:MerchantGroupRepository, @Inject val merchantGroupMemberRespository:MerchantGroupMemberRepository):MerchantGroupResource{
    override fun getGroup(userId: String, groupId: String): Response {
        if(log.isInfoEnabled){
            log.info("Request received to load group:{} for user:{}",groupId,userId)
        }
        val merchantGroup = merchantGroupRepo.findByIdAndConsumerId(groupId.toLong(),userId.toLong())
        return merchantGroup?.let { Response.ok(it.morph()).build() } ?: Response.status(Response.Status.NOT_FOUND).build()
    }

    val log:Logger = if(LoggerFactory.getLogger(this.javaClass) == null) NOPLogger.NOP_LOGGER else LoggerFactory.getLogger(this.javaClass)

    fun com.tft.services.merchantgroups.core.data.model.Merchant.morph() =
        Merchant(this.id.toString(),this.merchantName,this.merchant.email,this.merchant.mobile)

    fun com.tft.services.merchantgroups.core.data.model.MerchantGroup.morph() = MerchantGroup(this.id.toString(),this.name,this.merchants.map { it.morph() })



    override fun createGroup(userId: String, merchantGroup: MerchantGroup): Response {
        if(log.isInfoEnabled){
            log.info("Request received to create merchantGroup:$merchantGroup for User:$userId")
        }
        //Check whether merchants-consumer accounts are already present
        val merchantsInDB = mapMerchansToDB(merchantGroup.merchants)
        val merchantGroupInDB = com.tft.services.merchantgroups.core.data.model.MerchantGroup(consumerId = userId.toLong(),name = merchantGroup.groupName, merchants = merchantsInDB)
        merchantsInDB.forEach { it.merchantGroup = merchantGroupInDB }
        //log.info("Going to persist: $merchantGroupInDB")
        val merchantGroupUpdated = merchantGroupRepo.save(merchantGroupInDB)
        return Response.ok(merchantGroupUpdated.morph()).build()
    }

    private fun  mapMerchansToDB(merchants: List<Merchant>): MutableList<com.tft.services.merchantgroups.core.data.model.Merchant> {
        return merchants.map {
            val merchantConsumer:Consumer = gatherMerchantConsumer(it)
            com.tft.services.merchantgroups.core.data.model.Merchant(merchantName = it.name,merchant = merchantConsumer)
        }.toMutableList()
    }

    private fun  gatherMerchantConsumer(merchant: Merchant): Consumer {
        val merchantConsumer = if(merchant.email != null) consumerRepo.findByEmail(merchant.email) else if(merchant.mobile != null) consumerRepo.findByMobile(merchant.mobile) else null
        return if(merchantConsumer != null) merchantConsumer else consumerRepo.save(Consumer( email = merchant.email, mobile = merchant.mobile, status = 0L))
    }

    override fun getGroups(userId: String, pageSize: Int, page: Int): Response {
        if(log.isInfoEnabled){
            log.info("Request received to gather merchants groups setup for user:$userId page:$page size:$pageSize")
        }
        val merchantGroupsInDB = merchantGroupRepo.findByConsumerId(userId.toLong())
        return Response.ok(merchantGroupsInDB.map { it.morph() }).build()
    }

    override fun updateGroup(userId: String, groupId: String, merchantGroup: MerchantGroup) : Response{
        if(log.isInfoEnabled){
            log.info("Request received to update group:{} for user:{}",groupId,userId)
        }
        val merchantGroupFromDB = merchantGroupRepo.findByIdAndConsumerId(groupId.toLong(),userId.toLong())
        merchantGroupFromDB ?: return Response.status(Response.Status.NOT_FOUND).build()
        merchantGroupFromDB.name = merchantGroup.groupName
        merchantGroupFromDB.merchants.forEach {
            if(log.isInfoEnabled){
                log.info("Deleting merchant group member:{}",it.id)
            }
        }
        merchantGroupFromDB.merchants.clear()
        merchantGroupRepo.save(merchantGroupFromDB)
        val merchantsInDB = mapMerchansToDB(merchantGroup.merchants)
        merchantsInDB.forEach { it.merchantGroup = merchantGroupFromDB }
        merchantGroupMemberRespository.save(merchantsInDB)
        return getGroup(userId,groupId)
    }

    override fun deleteGroup(userId: String, groupId: String): Response {
        try{
            merchantGroupRepo.deleteByIdAndConsumerId(groupId.toLong(),userId.toLong())
        }
        catch(e: EmptyResultDataAccessException){
            if(log.isInfoEnabled){
                log.info("Attempt to delete an non-existing merchant-group: {} for user:{}",groupId,userId)
            }
        }
        return Response.ok().build()
    }

}