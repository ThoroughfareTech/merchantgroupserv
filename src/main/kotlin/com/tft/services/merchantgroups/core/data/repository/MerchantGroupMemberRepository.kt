package com.tft.services.merchantgroups.core.data.repository

import com.tft.services.merchantgroups.core.data.model.Merchant
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.transaction.annotation.Transactional

/**
 * Created by chanagarajan on 4/17/17.
 */

interface MerchantGroupMemberRepository : PagingAndSortingRepository<Merchant,Long>