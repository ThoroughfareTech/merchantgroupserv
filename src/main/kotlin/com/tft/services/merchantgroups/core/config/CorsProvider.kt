package com.tft.services.merchantgroups.core.config

import org.jboss.resteasy.plugins.interceptors.CorsFilter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import javax.inject.Named
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerResponseContext
import javax.ws.rs.ext.Provider

@Provider
@Named
class CorsProvider : CorsFilter(){
    val logger: Logger = LoggerFactory.getLogger(CorsProvider::class.java)
    init {
        super.getAllowedOrigins().add("*")
        logger.info("CORS Filter setup")
    }

    override fun filter(requestContext: ContainerRequestContext?, responseContext: ContainerResponseContext?) {
        super.filter(requestContext, responseContext)
        responseContext?.getHeaders()?.getFirst("Access-Control-Allow-Origin")?:responseContext?.getHeaders()?.add("Access-Control-Allow-Origin", "*")
        responseContext?.getHeaders()?.add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
        responseContext?.getHeaders()?.add("Access-Control-Allow-Credentials", "true")
        responseContext?.getHeaders()?.add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD, PATCH")
        responseContext?.getHeaders()?.add("Access-Control-Max-Age", "1209600")
    }
}