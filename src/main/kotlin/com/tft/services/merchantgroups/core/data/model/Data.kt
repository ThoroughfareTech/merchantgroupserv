package com.tft.services.merchantgroups.core.data.model

import java.math.BigInteger
import javax.persistence.*

/**
 * Created by chanagarajan on 4/14/17.
 */

@Table(name = "consumer", schema = "consumers") @Entity data class Consumer(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long? = null, val email:String? = null, val mobile:String? = null, val status:Long? = 0)

@Table(name = "merchant_group_member", schema = "consumers") @Entity data class Merchant(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long? = null, @ManyToOne @JoinColumn(name="merchant_group_id", referencedColumnName = "id") var merchantGroup:MerchantGroup? = null, val merchantName: String, @JoinColumn(name="merchant_id", referencedColumnName = "id", nullable = false) @OneToOne(optional = false) val merchant: Consumer )

@Table(name="merchant_group", schema = "consumers") @Entity data class MerchantGroup(@Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id:Long? = null, val consumerId: Long, var name:String, @OneToMany(fetch = FetchType.EAGER, cascade = arrayOf(CascadeType.ALL), orphanRemoval = true, mappedBy = "merchantGroup") val merchants:MutableList<Merchant>)