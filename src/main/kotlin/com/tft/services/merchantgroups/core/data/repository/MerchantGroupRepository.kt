package com.tft.services.merchantgroups.core.data.repository

import com.tft.services.merchantgroups.core.data.model.MerchantGroup
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.transaction.annotation.Transactional

/**
 * Created by chanagarajan on 4/17/17.
 */

interface MerchantGroupRepository : PagingAndSortingRepository<MerchantGroup,Long>{
    fun findByConsumerId(consumerId : Long) : List<MerchantGroup>
    fun findByIdAndConsumerId(id: Long, consumerId:Long) : MerchantGroup?

    @Transactional
    fun deleteByIdAndConsumerId(id:Long, consumerId:Long)
}