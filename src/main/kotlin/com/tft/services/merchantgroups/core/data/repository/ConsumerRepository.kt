package com.tft.services.merchantgroups.core.data.repository

import com.tft.services.merchantgroups.core.data.model.Consumer
import org.springframework.data.repository.CrudRepository
import java.math.BigInteger

/**
 * Created by chanagarajan on 4/17/17.
 */
interface ConsumerRepository : CrudRepository<Consumer,Long>{
    fun findByEmail(email:String):Consumer
    fun findByMobile(mobile:String):Consumer
}
