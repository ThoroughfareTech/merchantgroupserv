package com.tft.services.merchantgroups.core.config
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.jboss.resteasy.plugins.providers.jackson.ResteasyJackson2Provider
import org.slf4j.LoggerFactory
import javax.inject.Named
import javax.ws.rs.Consumes
import javax.ws.rs.Produces
import javax.ws.rs.ext.*

@Provider
@Consumes("application/*+json", "text/json")
@Produces("application/*+json", "text/json")
@Named
class JacksonKotlinModuleJsonProvider : ResteasyJackson2Provider() {
    val logger = LoggerFactory.getLogger(JacksonKotlinModuleJsonProvider::class.java)
    val objectMapper = ObjectMapper().let { it.registerModule(KotlinModule())
    super.setMapper(it)
    logger.info("Setup JacksonKotlinModuleJsonProvider as the Json Provider successfully")}
}
