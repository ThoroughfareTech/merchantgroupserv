package com.tft.services.merchantgroups

/**
 * Created by chanagarajan on 4/14/17.
 */
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import javax.ws.rs.ApplicationPath
import javax.ws.rs.ext.ContextResolver

@SpringBootApplication(scanBasePackages = arrayOf("com.tft.services.merchantgroups"))
@ApplicationPath("/v1/users/")
class Application:javax.ws.rs.core.Application()
fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}