package com.tft.services.merchantgroups.api.resources

import com.tft.services.merchantgroups.api.model.MerchantGroup
import com.tft.services.merchantgroups.api.model.MerchantGroupPage
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * Created by chanagarajan on 4/14/17.
 */

interface MerchantGroupResource{
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createGroup(@PathParam("user-id") userId:String, merchantGroup:MerchantGroup): Response

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getGroups(@PathParam("user-id") userId:String, @QueryParam("pageSize") pageSize:Int, @QueryParam("page") page:Int): Response

    @PUT
    @Path("{group-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun updateGroup(@PathParam("user-id") userId:String, @PathParam("group-id") groupId:String, merchantGroup: MerchantGroup): Response

    @GET
    @Path("{group-id}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getGroup(@PathParam("user-id") userId:String, @PathParam("group-id") groupId:String): Response

    @DELETE
    @Path("{group-id}")
    fun deleteGroup(@PathParam("user-id") userId:String, @PathParam("group-id") groupId:String): Response
}