package com.tft.services.merchantgroups.api.model

/**
 * Created by chanagarajan on 4/14/17.
 */
data class Merchant(val id:String? = null, val name:String, val email:String? = null, val mobile:String? = null)
data class MerchantGroup(val id:String? = null, val groupName:String, val merchants:List<Merchant>)
data class MerchantGroupPage(val page:Int=1, val totalPages:Int=1, val merchantGroups:List<MerchantGroup>)